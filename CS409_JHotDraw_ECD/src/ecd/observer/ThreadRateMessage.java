package ecd.observer;

public class ThreadRateMessage implements IMessage<Integer> {

	private final int value;
	
	public ThreadRateMessage(int value){
		this.value = value;
	}
	
	@Override
	public Integer getMessage() {
		return this.value;
	}
}
