package ecd.decorator;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import CH.ifa.draw.storable.StorableInput;
import ecd.figures.ElectricalFigure;
import ecd.figures.PulseSourceFigure;

/**
 * Decorator for {@link PulseSourceFigure}. Attached animation responsibilities
 * cause the pulse source to vary its output and change colour at a frequency
 * dictated by the user or the default of 1Hz.
 * 
 * @author Sean
 */
public class PulseSourceDecorator extends ElectricalDecorator {

	private static final long serialVersionUID = -1072971149364455681L;

	private static final String FREQUENCY = "Frequency (Hz):";
	private static final int DEFAULT_PERIOD = 1000,
			DEFAULT_FREQ = 1000 / (DEFAULT_PERIOD);

	private PulseSourceFigure fig;

	// To allow saving to occur, must be transient but when loaded would then be
	// null.
	private transient ScheduledExecutorService exec;
	private transient Future<?> thread;

	static {
		DecoratorFactory.getInstance().registerDecorator(
				PulseSourceFigure.class, PulseSourceDecorator.class);
	}

	public PulseSourceDecorator(PulseSourceFigure fig) {
		super(fig);
		this.fig = fig;
		this.exec = Executors.newSingleThreadScheduledExecutor();
		this.fig.setAttribute(ElectricalFigure.TEXT_ATTRIB,
				Integer.toString(DEFAULT_FREQ));
		this.fig.setProperty(FREQUENCY, Integer.toString(DEFAULT_FREQ));
	}

	public PulseSourceDecorator() {
		super();
	}

	@Override
	public void animationStep() {
		if (this.exec == null)
			this.exec = Executors.newSingleThreadScheduledExecutor();
		if (this.thread == null) {
			final float pulseFreq = (1 / Float.parseFloat(getProperties().get(
					FREQUENCY)));
			final int period = Math.round(pulseFreq * 1000);
			this.thread = this.exec.scheduleAtFixedRate(new Runnable() {
				@Override
				public void run() {
					LogicLevel input = LogicLevel.toggle(fig.getOutput());
					fig.setOutput(input);

					if (input.equals(LogicLevel.HIGH)) {
						fig.setAttribute(ElectricalFigure.FILL_ATTRIB,
								PulseSourceFigure.ON_COLOR);
					} else if (input.equals(LogicLevel.LOW)) {
						fig.setAttribute(ElectricalFigure.FILL_ATTRIB,
								PulseSourceFigure.OFF_COLOR);
					}
				}
			}, period, period, TimeUnit.MILLISECONDS);
		}
	}

	@Override
	public void setProperty(String name, String property) {
		try {
			float freq = Float.parseFloat(property);
			if (freq > 0) {
				this.fig.setAttribute(ElectricalFigure.TEXT_ATTRIB, property);
				super.setProperty(name, property);
			}
		} catch (NumberFormatException e) { // Catch invalid input
		}
	}

	@Override
	public void ceaseAnimation() {
		if (this.thread != null) {
			this.thread.cancel(true);
			this.thread = null;
		}
		return;
	}

	@Override
	public void read(StorableInput dr) throws IOException {
		super.read(dr);
		this.fig = (PulseSourceFigure) fComponent;
	}
}
