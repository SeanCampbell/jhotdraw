package ecd.figures;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import CH.ifa.draw.figure.GroupFigure;
import CH.ifa.draw.figure.RectangleFigure;
import CH.ifa.draw.figure.TextFigure;
import CH.ifa.draw.framework.Figure;
import CH.ifa.draw.storable.StorableInput;
import CH.ifa.draw.storable.StorableOutput;

public abstract class ElectricalFigure extends GroupFigure implements
		IElectricalFigure {

	private static final long serialVersionUID = -2552565088459387632L;

	public static final String FILL_ATTRIB = "FillColor", TEXT_ATTRIB = "TEXT";
	private static final Color BOX_COLOR = new Color(255, 191, 0); // AMBER

	// DEFAULT_ELECTRICAL_BOX
	private static final int BOX_L = 38, BOX_C = 36;

	// For prototype, can't copy the lists snce it may invalidate the connection
	// properties of the connected objects
	private transient List<IElectricalFigure> predecessors, successors;
	private final Map<String, String> properties;

	public ElectricalFigure() {
		super();
		// Template
		super.addAll(this.buildElectricalBox(new Vector<>(this.getGeneralLook())));

		// This is a constructor invariant but the usage of serialisation in the
		// above application
		// forces the usage of null checks.
		this.predecessors = new Vector<>();
		this.successors = new Vector<>();

		this.properties = new HashMap<>();
	}

	// Variable method in template pattern
	protected abstract Collection<Figure> getGeneralLook();

	// Build the look; scale everything children offer within the box.
	private Vector<Figure> buildElectricalBox(final List<Figure> boxComponents) {
		final Vector<Figure> figures = new Vector<>();
		final Figure electricalBox = new RectangleFigure(new Point(0, 0),
				new Point(BOX_L, BOX_L));
		electricalBox.setAttribute(FILL_ATTRIB, BOX_COLOR);
		final Rectangle disp = electricalBox.displayBox();

		// Calculate scaling required to fit in the box
		double sf = 1.0;
		for (Figure f : boxComponents) {
			// Shift top-left corner to (0, 0)
			Rectangle rect = new Rectangle(f.displayBox());
			rect.setLocation(0, 0);
			// Shape larger than electrical box constraints
			if (!disp.contains(rect)) {
				sf = Math.min(
						sf,
						Math.min(BOX_C / rect.getWidth(),
								BOX_C / rect.getHeight()));
			}
		}

		// Transform the shapes
		AffineTransform af = new AffineTransform();
		af.scale(sf, sf);
		for (Figure f : boxComponents) {
			Rectangle rect = new Rectangle(af.createTransformedShape(
					f.displayBox()).getBounds());
			rect.setLocation(BOX_L / 2 - rect.width / 2, BOX_L / 2
					- rect.height / 2);
			f.displayBox(rect);
		}

		figures.add(electricalBox);
		figures.addAll(boxComponents);
		return figures;
	}

	@Override
	public void basicDisplayBox(Point origin, Point corner) {
		Rectangle rect = this.displayBox();
		this.moveBy((int) origin.getX() - rect.x, (int) origin.getY() - rect.y);
	}

	@Override
	public Object getAttribute(String name) {
		Object ret = null;
		Vector<Figure> figs = super.fFigures;
		if (figs.size() > 1)
			ret = figs.get(1).getAttribute(name);
		return ret;
	}

	// Set attribute of all figures except the Electrical box.
	// Additionally, allows hacky setting of text in text figures.
	@Override
	public void setAttribute(String name, Object value) {
		Enumeration<Figure> figs = figures();
		if (name.equals(ElectricalFigure.TEXT_ATTRIB)) {
			while (figs.hasMoreElements()) {
				Figure fig = figs.nextElement();
				if (fig instanceof TextFigure) {
					this.setTextAndCentre((TextFigure) fig, (String) value);
				}
			}
		} else {
			while (figs.hasMoreElements()) {
				Figure fig = figs.nextElement();
				if (!fig.equals(super.fFigures.get(0))) {
					fig.setAttribute(name, value);
				}
			}
		}
	}

	// Private helper for setting the text and centring on the casing
	private void setTextAndCentre(TextFigure fig, String value) {
		fig.setText(value);
		Rectangle rect = new Rectangle(fig.displayBox()), disp = super.fFigures
				.get(0).displayBox();
		rect.setLocation(disp.x + disp.width / 2 - rect.width / 2, disp.y
				+ disp.height / 2 - rect.height / 2);
		fig.displayBox(rect);
	}

	@Override
	public Collection<IElectricalFigure> getPredecessors() {
		checkPredecessorsNull();
		return new Vector<IElectricalFigure>(this.predecessors);
	}

	@Override
	public Collection<IElectricalFigure> getSuccessors() {
		checkSuccessorsNull();
		return new Vector<IElectricalFigure>(this.successors);
	}

	// Necessary due to serialisation violating constructor invariants
	private void checkPredecessorsNull() {
		if (this.predecessors == null)
			this.predecessors = new Vector<>();
	}

	// Necessary due to serialisation violating constructor invariants
	private void checkSuccessorsNull() {
		if (this.successors == null)
			this.successors = new Vector<>();
	}

	@Override
	public void addPredecessor(IElectricalFigure pred) {
		checkPredecessorsNull();
		this.predecessors.add(pred);
	}

	@Override
	public void addSuccessor(IElectricalFigure succ) {
		checkSuccessorsNull();
		this.successors.add(succ);
	}

	@Override
	public void removePredecessor(IElectricalFigure pred) {
		checkPredecessorsNull();
		this.predecessors.remove(pred);
	}

	@Override
	public void removeSuccessor(IElectricalFigure succ) {
		checkSuccessorsNull();
		this.successors.remove(succ);
	}

	@Override
	public boolean canConnect() {
		return canConnectInput() || canConnectOutput();
	}

	@Override
	public boolean canConnectInput() {
		return true;
	}

	@Override
	public boolean canConnectOutput() {
		return true;
	}

	@Override
	public void setOutput(LogicLevel output) {
		throw new UnsupportedOperationException();
	}

	@Override
	public LogicLevel getOutput() {
		return LogicLevel.LOW;
	}

	@Override
	public Map<String, String> getProperties() {
		return new HashMap<>(this.properties);
	}

	@Override
	public void setProperty(String name, String value) {
		this.properties.put(name, value);
	}

	@Override
	public boolean hasCycle(IElectricalFigure fig) {
		if (fig == this)
			return true;
		Iterator<IElectricalFigure> iter = this.getPredecessors().iterator();
		while (iter.hasNext()) {
			if (iter.next().hasCycle(fig)) {
				return true;
			}
		}
		return false;
	}

	// Additionally writes the preoperties of the figure to file.
	@Override
	public void write(StorableOutput dw) {
		dw.writeInt(fFigures.size() + 1);
		Enumeration<Figure> k = fFigures.elements();
		while (k.hasMoreElements())
			dw.writeStorable(k.nextElement());
		dw.writeString("READ_PROPERTIES");
		for (Entry<String, String> entry : this.properties.entrySet()) {
			dw.writeString(entry.getKey());
			dw.writeString(entry.getValue());
		}
		dw.writeString("END_PROPERTIES");
	}

	// Additionally reads the properties of the figure from file.
	@Override
	public void read(StorableInput dr) throws IOException {
		int size = dr.readInt();
		fFigures = new Vector<Figure>(size);
		for (int i = 0; i < size - 1; i++)
			add((Figure) dr.readStorable());
		if (dr.readString().equals("READ_PROPERTIES")) {
			String key, value;
			boolean control = true;
			while (control) {
				key = dr.readString();
				if (!key.equals("END_PROPERTIES")) {
					value = dr.readString();
					this.properties.put(key, value);
				} else
					control = false;
			}
		}
	}
}
