package ecd.tools;

import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;

import CH.ifa.draw.framework.DrawingView;
import CH.ifa.draw.framework.Figure;
import CH.ifa.draw.tool.CreationTool;

/**
 * A custom creation tool which attempts to prevent figures from being added
 * directly on top of one another. In the case of the current selection causing
 * an overlap, it will attempt to place the new figure to the bottom right of
 * the overlapped figure, occurring successively.
 * 
 * @author Sean
 */
public class CustomCreationTool extends CreationTool {

	protected CustomCreationTool(DrawingView view) {
		super(view);
	}

	public CustomCreationTool(DrawingView view, Figure prototype) {
		super(view, prototype);
	}

	private static final int LIMITER = 10;

	@Override
	public void mouseUp(MouseEvent e, int x, int y) {
		if (SwingUtilities.isRightMouseButton(e)) {
			editor().toolDone();
		}
	}

	@Override
	public void mouseDown(MouseEvent e, int x, int y) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			this.isLeftClick(e, x, y);
		}
	}

	private void isLeftClick(MouseEvent e, int x, int y) {
		// Instance variables
		boolean atLeastOnce = true;
		int current = 0;
		Figure created = null;
		java.awt.Dimension dim = view().getSize();
		// Bounded loop
		while (atLeastOnce && current != LIMITER) {
			super.mouseDown(e, x, y);
			created = createdFigure();
			view().remove(created);
			Figure intersection = drawing().findFigure(created.displayBox());
			if (intersection != null) {
				java.awt.Rectangle rect = intersection.displayBox(), rect2 = created
						.displayBox();
				x = Math.min(dim.width - rect2.width, rect.x + rect.width);
				y = Math.min(dim.height - rect2.height, rect.y + rect.height);
				current++;
			} else {
				java.awt.Rectangle rect = created.displayBox();
				x = Math.min(dim.width - rect.width, rect.x);
				y = Math.min(dim.height - rect.height, rect.y);
				// Code dupe: A few ways; this seems cleanest, i.e. no breaks
				super.mouseDown(e, x, y);
				created = createdFigure();
				view().remove(created);
				atLeastOnce = false;
			}
		}
		view().add(created);
	}
}
