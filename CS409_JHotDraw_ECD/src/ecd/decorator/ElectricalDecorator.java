package ecd.decorator;

import java.util.Collection;
import java.util.Map;

import ecd.app.IAnimatable;
import ecd.figures.IElectricalFigure;
import CH.ifa.draw.figure.DecoratorFigure;
import CH.ifa.draw.framework.Figure;

/**
 * Typical decorator, delegating calls to the encased {@link IElectricalFigure}.
 * 
 * @author Sean
 */
public abstract class ElectricalDecorator extends DecoratorFigure implements
		IElectricalFigure, IAnimatable {

	private static final long serialVersionUID = -8951905863647038079L;

	public ElectricalDecorator(Figure fig) {
		super(fig);
	}

	public ElectricalDecorator() {
		super();
	}

	@Override
	public Collection<IElectricalFigure> getPredecessors() {
		Collection<IElectricalFigure> ret = null;
		if (super.fComponent instanceof IElectricalFigure)
			ret = ((IElectricalFigure) super.fComponent).getPredecessors();
		return ret;
	}

	@Override
	public Collection<IElectricalFigure> getSuccessors() {
		Collection<IElectricalFigure> ret = null;
		if (super.fComponent instanceof IElectricalFigure)
			ret = ((IElectricalFigure) super.fComponent).getSuccessors();
		return ret;
	}

	@Override
	public void addPredecessor(IElectricalFigure pred) {
		if (super.fComponent instanceof IElectricalFigure)
			((IElectricalFigure) super.fComponent).addPredecessor(pred);
	}

	@Override
	public void addSuccessor(IElectricalFigure succ) {
		if (super.fComponent instanceof IElectricalFigure)
			((IElectricalFigure) super.fComponent).addSuccessor(succ);
	}

	@Override
	public void removePredecessor(IElectricalFigure pred) {
		if (super.fComponent instanceof IElectricalFigure)
			((IElectricalFigure) super.fComponent).removePredecessor(pred);
	}

	@Override
	public void removeSuccessor(IElectricalFigure succ) {
		if (super.fComponent instanceof IElectricalFigure)
			((IElectricalFigure) super.fComponent).removeSuccessor(succ);
	}

	@Override
	public boolean canConnectInput() {
		boolean ret = false;
		if (super.fComponent instanceof IElectricalFigure)
			ret = ((IElectricalFigure) super.fComponent).canConnectInput();
		return ret;
	}

	@Override
	public boolean canConnectOutput() {
		boolean ret = false;
		if (super.fComponent instanceof IElectricalFigure)
			ret = ((IElectricalFigure) super.fComponent).canConnectOutput();
		return ret;
	}

	@Override
	public LogicLevel getOutput() {
		LogicLevel ret = LogicLevel.UNDEFINED;
		if (super.fComponent instanceof IElectricalFigure)
			ret = ((IElectricalFigure) super.fComponent).getOutput();
		return ret;
	}

	@Override
	public Map<String, String> getProperties() {
		Map<String, String> ret = null;
		if (super.fComponent instanceof IElectricalFigure)
			ret = ((IElectricalFigure) super.fComponent).getProperties();
		return ret;
	}

	@Override
	public void setProperty(String name, String property) {
		if (super.fComponent instanceof IElectricalFigure)
			((IElectricalFigure) super.fComponent).setProperty(name, property);
	}

	@Override
	public void setOutput(LogicLevel output) {
		if (super.fComponent instanceof IElectricalFigure)
			((IElectricalFigure) super.fComponent).setOutput(output);
	}

	@Override
	public boolean hasCycle(IElectricalFigure fig) {
		if (super.fComponent instanceof IElectricalFigure)
			((IElectricalFigure) super.fComponent).hasCycle(fig);
		return false;
	}
}
