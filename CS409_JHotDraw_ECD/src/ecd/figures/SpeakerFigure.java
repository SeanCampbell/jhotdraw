package ecd.figures;

import java.awt.Color;
import java.awt.Point;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import CH.ifa.draw.contrib.TriangleFigure;
import CH.ifa.draw.figure.RectangleFigure;
import CH.ifa.draw.framework.Figure;

/**
 * A speaker which accepts a single input.
 * 
 * @author Sean
 */
public class SpeakerFigure extends ElectricalFigure {

	private static final long serialVersionUID = -2864885200619926220L;

	private static final int SQUARE_L = 28, INPUT_COUNT = 1;

	public SpeakerFigure() {
		super();
	}

	protected Collection<Figure> getGeneralLook() {
		final List<Figure> figures = new Vector<>();
		// figures.get(0).setAttribute(ElectricalFigure.FILL_ATTRIB,
		// Color.BLACK);
		TriangleFigure fig = new TriangleFigure(new Point(SQUARE_L, SQUARE_L),
				new Point(0, 0));
		fig.rotate(90);
		figures.add(fig);
		figures.add(new RectangleFigure(new Point(0, 0), new Point(SQUARE_L,
				SQUARE_L / 2)));
		for (Figure f : figures)
			f.setAttribute(FILL_ATTRIB, Color.BLACK);
		return figures;
	}

	@Override
	public void addSuccessor(IElectricalFigure pred) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeSuccessor(IElectricalFigure pred) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean canConnectInput() {
		return INPUT_COUNT != super.getPredecessors().size();
	}

	@Override
	public boolean canConnectOutput() {
		return false;
	}
}
