package ecd.strategies;

import ecd.figures.IElectricalFigure.LogicLevel;

public class Nand extends And {

	private static final long serialVersionUID = -5157997952393222508L;

	private static final String NAME = "NAND";
	private static final Integer DEF_INPUT_PINS = 2;

	static {
		StrategyFactory.getInstance().registerStrategy(Nand.NAME, Nand.class);
		StrategyFactory.getInstance().registerInput(Nand.NAME, DEF_INPUT_PINS);
	}

	@Override
	protected LogicLevel getStrategyOutput(LogicLevel[] inputs) {
		return LogicLevel.toggle(super.getStrategyOutput(inputs));
	}

	@Override
	public String strategyName() {
		return Nand.NAME;
	}
}
