package ecd.test;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;

import ecd.figures.IElectricalFigure.LogicLevel;
import ecd.strategies.IStrategy;
import ecd.strategies.IStrategyFactory;
import ecd.strategies.StrategyFactory;

/**
 * Simple test class for all of extensions of {@link Strategy}.
 * 
 * @author Sean
 */
public class TStrategy {

	private static final IStrategyFactory factory = StrategyFactory
			.getInstance();

	private static final LogicLevel inputs[][] = {
			{ LogicLevel.LOW, LogicLevel.LOW },
			{ LogicLevel.LOW, LogicLevel.HIGH },
			{ LogicLevel.HIGH, LogicLevel.LOW },
			{ LogicLevel.HIGH, LogicLevel.HIGH },
			{ LogicLevel.UNDEFINED, LogicLevel.UNDEFINED } };

	private static final Map<LogicLevel[], LogicLevel> expectedNotOutput = new HashMap<>(),
			expectedAndOutput = new HashMap<>(),
			expectedOrOutput = new HashMap<>(),
			expectedNandOutput = new HashMap<>(),
			expectedNorOutput = new HashMap<>(),
			expectedXorOutput = new HashMap<>(),
			expectedXnorOutput = new HashMap<>();

	static {
		// Set-up expected not values
		expectedNotOutput.put(new LogicLevel[] { LogicLevel.LOW },
				LogicLevel.HIGH);
		expectedNotOutput.put(new LogicLevel[] { LogicLevel.HIGH },
				LogicLevel.LOW);
		expectedNotOutput.put(new LogicLevel[] { LogicLevel.UNDEFINED },
				LogicLevel.UNDEFINED);
		// Set-up expected and values
		expectedAndOutput.put(inputs[0], LogicLevel.LOW);
		expectedAndOutput.put(inputs[1], LogicLevel.LOW);
		expectedAndOutput.put(inputs[2], LogicLevel.LOW);
		expectedAndOutput.put(inputs[3], LogicLevel.HIGH);
		expectedAndOutput.put(inputs[4], LogicLevel.UNDEFINED);
		// Set-up expected or values
		expectedOrOutput.put(inputs[0], LogicLevel.LOW);
		expectedOrOutput.put(inputs[1], LogicLevel.HIGH);
		expectedOrOutput.put(inputs[2], LogicLevel.HIGH);
		expectedOrOutput.put(inputs[3], LogicLevel.HIGH);
		expectedOrOutput.put(inputs[4], LogicLevel.UNDEFINED);
		// Set-up expected nand values
		expectedNandOutput.put(inputs[0], LogicLevel.HIGH);
		expectedNandOutput.put(inputs[1], LogicLevel.HIGH);
		expectedNandOutput.put(inputs[2], LogicLevel.HIGH);
		expectedNandOutput.put(inputs[3], LogicLevel.LOW);
		expectedNandOutput.put(inputs[4], LogicLevel.UNDEFINED);
		// Set-up expected nor values
		expectedNorOutput.put(inputs[0], LogicLevel.HIGH);
		expectedNorOutput.put(inputs[1], LogicLevel.LOW);
		expectedNorOutput.put(inputs[2], LogicLevel.LOW);
		expectedNorOutput.put(inputs[3], LogicLevel.LOW);
		expectedNorOutput.put(inputs[4], LogicLevel.UNDEFINED);
		// Set-up expected xor values
		expectedXorOutput.put(inputs[0], LogicLevel.LOW);
		expectedXorOutput.put(inputs[1], LogicLevel.HIGH);
		expectedXorOutput.put(inputs[2], LogicLevel.HIGH);
		expectedXorOutput.put(inputs[3], LogicLevel.LOW);
		expectedXorOutput.put(inputs[4], LogicLevel.UNDEFINED);
		// Set-up expected xnor values
		expectedXnorOutput.put(inputs[0], LogicLevel.HIGH);
		expectedXnorOutput.put(inputs[1], LogicLevel.LOW);
		expectedXnorOutput.put(inputs[2], LogicLevel.LOW);
		expectedXnorOutput.put(inputs[3], LogicLevel.HIGH);
		expectedXnorOutput.put(inputs[4], LogicLevel.UNDEFINED);
	}

	// Effectively, test each strategy using the above maps.
	@Test
	public void testNot() {
		try {
			IStrategy not = factory.getStrategy("NOT");
			for (Entry<LogicLevel[], LogicLevel> entry : expectedNotOutput
					.entrySet()) {
				assertEquals(entry.getValue(), not.execute(entry.getKey()));
			}
		} catch (InstantiationException e) {
			fail("Obtained InstantiationException for strategy: NOT");
		} catch (IllegalAccessException e) {
			fail("Obtained IllegalAccessException for strategy: NOT");
		}
	}

	@Test
	public void testAnd() {
		try {
			IStrategy and = factory.getStrategy("AND");
			for (Entry<LogicLevel[], LogicLevel> entry : expectedAndOutput
					.entrySet()) {
				assertEquals(entry.getValue(), and.execute(entry.getKey()));
			}
		} catch (InstantiationException e) {
			fail("Obtained InstantiationException for strategy: AND");
		} catch (IllegalAccessException e) {
			fail("Obtained IllegalAccessException for strategy: AND");
		}
	}

	@Test
	public void testOr() {
		try {
			IStrategy and = factory.getStrategy("OR");
			for (Entry<LogicLevel[], LogicLevel> entry : expectedOrOutput
					.entrySet()) {
				assertEquals(entry.getValue(), and.execute(entry.getKey()));
			}
		} catch (InstantiationException e) {
			fail("Obtained InstantiationException for strategy: OR");
		} catch (IllegalAccessException e) {
			fail("Obtained IllegalAccessException for strategy: OR");
		}
	}

	@Test
	public void testNand() {
		try {
			IStrategy and = factory.getStrategy("NAND");
			for (Entry<LogicLevel[], LogicLevel> entry : expectedNandOutput
					.entrySet()) {
				assertEquals(entry.getValue(), and.execute(entry.getKey()));
			}
		} catch (InstantiationException e) {
			fail("Obtained InstantiationException for strategy: NAND");
		} catch (IllegalAccessException e) {
			fail("Obtained IllegalAccessException for strategy: NAND");
		}
	}

	@Test
	public void testNor() {
		try {
			IStrategy and = factory.getStrategy("NOR");
			for (Entry<LogicLevel[], LogicLevel> entry : expectedNorOutput
					.entrySet()) {
				assertEquals(entry.getValue(), and.execute(entry.getKey()));
			}
		} catch (InstantiationException e) {
			fail("Obtained InstantiationException for strategy: NOR");
		} catch (IllegalAccessException e) {
			fail("Obtained IllegalAccessException for strategy: NOR");
		}
	}

	@Test
	public void testXor() {
		try {
			IStrategy and = factory.getStrategy("XOR");
			for (Entry<LogicLevel[], LogicLevel> entry : expectedXorOutput
					.entrySet()) {
				assertEquals(entry.getValue(), and.execute(entry.getKey()));
			}
		} catch (InstantiationException e) {
			fail("Obtained InstantiationException for strategy: XOR");
		} catch (IllegalAccessException e) {
			fail("Obtained IllegalAccessException for strategy: XOR");
		}
	}

	@Test
	public void testXnor() {
		try {
			IStrategy and = factory.getStrategy("XNOR");
			for (Entry<LogicLevel[], LogicLevel> entry : expectedXnorOutput
					.entrySet()) {
				assertEquals(entry.getValue(), and.execute(entry.getKey()));
			}
		} catch (InstantiationException e) {
			fail("Obtained InstantiationException for strategy: XNOR");
		} catch (IllegalAccessException e) {
			fail("Obtained IllegalAccessException for strategy: XNOR");
		}
	}

	/*
	 * @Test public void testEquals() throws Exception{ for(Entry<String,
	 * Class<? extends IStrategy>> entry : factory.getStrategies().entrySet()){
	 * testObjectProperties(factory.getStrategy(entry.getKey())); } }
	 * 
	 * private void testObjectProperties(IStrategy strat) throws Exception{
	 * //Test valid object is not null. assertFalse("Error: Object of " +
	 * strat.strategyName() + " equal to null.", strat.equals(null)); //Test
	 * reflexive property. assertTrue("Error: "+ strat.strategyName()
	 * +" is not reflexive.", strat.equals(strat)); //Test symmetric. IStrategy
	 * strat2 = factory.getStrategy(strat.strategyName());
	 * assertEquals("Error: "+strat.strategyName()+" is not symmetric.", strat,
	 * strat2); //Test transitive. IStrategy strat3 =
	 * factory.getStrategy(strat2.strategyName()); if(strat.equals(strat2) &&
	 * strat2.equals(strat3)) assertEquals("Error: "+ strat.strategyName()
	 * +" is not transitive.", strat, strat3); //Test consistency.
	 * assertTrue("Error: "+
	 * strat.strategyName()+" is not consistent.",strat.equals
	 * (strat2)==strat.equals(strat2)); }
	 */
}
