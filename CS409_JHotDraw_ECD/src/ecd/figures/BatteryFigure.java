package ecd.figures;

import java.awt.Color;
import java.awt.Point;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import CH.ifa.draw.figure.EllipseFigure;
import CH.ifa.draw.framework.Figure;

/**
 * A battery which continuously outputs a high {@link LogicLevel}.
 * 
 * @author Sean
 */
public class BatteryFigure extends ElectricalFigure {

	private static final long serialVersionUID = 1459705435256142168L;

	private static final Color SOURCE_INNER_COLOR = Color.RED,
			SOURCE_OUTER_COLOR = new Color(161, 0, 0);

	// Source constants
	private static final int OUTER_DIAM = 32, INNER_OFFSET = 2;

	public BatteryFigure() {
		super();
	}

	protected Collection<Figure> getGeneralLook() {
		final List<Figure> figures = new Vector<>();
		figures.add(new EllipseFigure(new Point(0, 0), new Point(OUTER_DIAM,
				OUTER_DIAM)));
		figures.get(0).setAttribute(ElectricalFigure.FILL_ATTRIB,
				SOURCE_OUTER_COLOR);
		figures.add(new EllipseFigure(new Point(INNER_OFFSET, INNER_OFFSET),
				new Point(OUTER_DIAM - INNER_OFFSET, OUTER_DIAM - INNER_OFFSET)));
		figures.get(1).setAttribute(ElectricalFigure.FILL_ATTRIB,
				SOURCE_INNER_COLOR);
		return figures;
	}

	@Override
	public void addPredecessor(IElectricalFigure pred) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removePredecessor(IElectricalFigure pred) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean canConnectInput() {
		return false;
	}

	@Override
	public LogicLevel getOutput() {
		return LogicLevel.HIGH;
	}
}
