package ecd.strategies;

import ecd.figures.IElectricalFigure.LogicLevel;

public class Or extends Strategy {

	private static final long serialVersionUID = 5711160309692051125L;

	private static final String NAME = "OR";
	private static final Integer DEF_INPUT_PINS = 2;

	static {
		StrategyFactory.getInstance().registerStrategy(Or.NAME, Or.class);
		StrategyFactory.getInstance().registerInput(Or.NAME, Or.DEF_INPUT_PINS);
	}

	@Override
	public String strategyName() {
		return Or.NAME;
	}

	@Override
	protected LogicLevel getStrategyOutput(LogicLevel[] inputs) {
		LogicLevel ret = LogicLevel.LOW;
		for (LogicLevel l : inputs) {
			if (l.equals(LogicLevel.HIGH)) {
				ret = LogicLevel.HIGH;
				break;
			}
		}
		return ret;
	}
}
