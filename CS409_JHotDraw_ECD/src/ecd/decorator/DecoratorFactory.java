package ecd.decorator;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ecd.figures.IElectricalFigure;

public class DecoratorFactory implements IDecoratorFactory {

	private static final String PACKAGE_NAME = "ecd.decorator.";

	private static IDecoratorFactory instance;
	private static final Map<Class<? extends IElectricalFigure>, Class<? extends IElectricalFigure>> registeredDecorators;

	// Class loader material
	static {
		registeredDecorators = new HashMap<>();
		try {
			Class.forName(PACKAGE_NAME + "BulbDecorator");
			Class.forName(PACKAGE_NAME + "SpeakerDecorator");
			Class.forName(PACKAGE_NAME + "PulseSourceDecorator");
			Class.forName(PACKAGE_NAME + "ElectricalDecorator");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	public static IDecoratorFactory getInstance() {
		if (instance == null) {
			instance = new DecoratorFactory();
		}
		return instance;
	}

	protected DecoratorFactory() {
	}

	public IElectricalFigure getDecorator(
			Class<? extends IElectricalFigure> item)
			throws InstantiationException, IllegalAccessException {
		if (!registeredDecorators.containsKey(item))
			throw new InstantiationException("Unknown item:\t" + item);
		return registeredDecorators.get(item).newInstance();
	}

	// Added in case future logic extensions take parameters
	public IElectricalFigure getDecorator(
			Class<? extends IElectricalFigure> item, Object... params)
			throws InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		if (!registeredDecorators.containsKey(item))
			throw new InstantiationException("Unknown item:\t" + item);
		Constructor<? extends IElectricalFigure> ctor = this.getConstructor(
				item, params);
		return ctor.newInstance(params);
	}

	private Constructor<? extends IElectricalFigure> getConstructor(
			Class<? extends IElectricalFigure> item, Object... args)
			throws InstantiationException {
		@SuppressWarnings("unchecked")
		Constructor<? extends IElectricalFigure>[] ctors = (Constructor<? extends IElectricalFigure>[]) registeredDecorators
				.get(item).getConstructors();

		for (Constructor<? extends IElectricalFigure> ctor : ctors) {
			if (ctor.getParameterTypes().length == args.length) {
				boolean possibleMatch = true;

				// Fetch all the classes corresponding to this constructor
				List<Class<?>> classes = new ArrayList<>(Arrays.asList(ctor
						.getParameterTypes()));

				for (int i = 0; i < args.length; i++) {
					Class<?> type = args[i].getClass();
					Class<?> ctor_type = classes.get(i);

					if (!ctor_type.isAssignableFrom(type)) {
						if (ctor_type.isPrimitive()) {
							possibleMatch = (int.class.equals(ctor_type) && Integer.class
									.equals(type))
									|| (double.class.equals(ctor_type))
									&& Double.class.equals(type)
									|| (long.class.equals(ctor_type) && Long.class
											.equals(type))
									|| (char.class.equals(ctor_type) && Character.class
											.equals(type))
									|| (short.class.equals(ctor_type) && Short.class
											.equals(type))
									|| (boolean.class.equals(ctor_type) && Boolean.class
											.equals(type))
									|| (byte.class.equals(ctor_type) && Byte.class
											.equals(type));
						} else if (ctor_type.equals(type))
							possibleMatch = true;
						else {
							possibleMatch = false;
							break;
						}
					}
				}
				if (possibleMatch)
					return ctor;
			}
		}
		throw new InstantiationException(
				"Could not find acceptable constructor");
	}

	@Override
	public void registerDecorator(Class<? extends IElectricalFigure> item,
			Class<? extends IElectricalFigure> decorator) {
		registeredDecorators.put(item, decorator);
	}
}
