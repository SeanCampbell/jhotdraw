package ecd.figures;

import java.awt.Color;
import java.util.Collection;

import CH.ifa.draw.figure.TextFigure;
import CH.ifa.draw.framework.Figure;

/**
 * Extension of a battery where the output can be set to a specific
 * {@link LogicLevel}.
 * 
 * @author Sean
 */
public class PulseSourceFigure extends BatteryFigure {

	private static final long serialVersionUID = 698477447973200333L;

	private static final String TEXT_COLOR = "TextColor",
			TEXT_SIZE = "FontSize";

	public static final Color TEXT_COLOR_ = Color.WHITE, ON_COLOR = Color.RED,
			OFF_COLOR = Color.BLACK;
	private static final int TEXT_SIZE_ = 9;

	private LogicLevel output;

	public PulseSourceFigure() {
		super();
		this.output = LogicLevel.HIGH;
	}

	protected Collection<Figure> getGeneralLook() {
		final Collection<Figure> figures = super.getGeneralLook();
		final TextFigure tf = new TextFigure();
		tf.setAttribute(TEXT_SIZE, TEXT_SIZE_);
		tf.setReadOnly(true);
		tf.setAttribute(TEXT_COLOR, TEXT_COLOR_);
		figures.add(tf);
		return figures;
	}

	@Override
	public void setOutput(LogicLevel output) {
		this.output = output;
	}

	@Override
	public LogicLevel getOutput() {
		return this.output;
	}
}
