package ecd.app;

/**
 * Interface defining responsibilities for anything seeking to be animatable.
 * This was constructed and employed rather than the framework interface due to
 * the fact that there existed no way to reliably signal the animation was about
 * to cease, necessary due to the multi-threading.
 * 
 * @author Sean
 */
public interface IAnimatable {
	/**
	 * Perform a step of the animation.
	 */
	void animationStep();

	/**
	 * Safely stop anything the animation steps have invoked, i.e. give
	 * implementations the chance to safely stop runnables.
	 */
	void ceaseAnimation();
}
