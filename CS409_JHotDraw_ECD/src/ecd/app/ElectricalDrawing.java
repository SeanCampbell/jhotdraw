package ecd.app;

import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;

import ecd.decorator.DecoratorFactory;
import ecd.decorator.ElectricalDecorator;
import ecd.decorator.IDecoratorFactory;
import ecd.figures.IElectricalFigure;
import CH.ifa.draw.figure.DecoratorFigure;
import CH.ifa.draw.framework.Figure;
import CH.ifa.draw.standard.StandardDrawing;

/**
 * The drawing for use by the application. Since it is an animatable drawing, it
 * attempts to attach animation decorators to figures which are to be added. The
 * replace operation was not supported as it was unknown how to perform it, thus
 * it could not be tested.
 * 
 * @author Sean
 */
public class ElectricalDrawing extends StandardDrawing implements IAnimatable {

	private static final long serialVersionUID = -2248079278788687218L;

	public ElectricalDrawing() {
		super();
	}

	// Interesting glitch(?) in StandardDrawing. This method is called multiple
	// times
	// upon the addition of one figure; adds then removes, adds then removes
	// then adds.
	@Override
	public synchronized Figure add(Figure figure) {
		if (!(figure instanceof IAnimatable)
				&& figure instanceof IElectricalFigure) {
			IElectricalFigure fig = (IElectricalFigure) figure;
			try {
				figure = this.getFactory().getDecorator(fig.getClass(), fig);
			} catch (InstantiationException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException e) {
				// Most likely no factory instance register
				// System.out.println("No registered decorator for:" +
				// figure.getClass().getSimpleName());
			}
		}
		return super.add(figure);
	}

	@Override
	public synchronized Figure remove(Figure figure) {
		Figure f = super.remove(figure);
		if (f instanceof ElectricalDecorator) {
			f = ((DecoratorFigure) f).peelDecoration();
		}
		return f;
	}

	@Override
	public synchronized void replace(Figure figure, Figure replacement) {
		throw new UnsupportedOperationException(
				"I don't support replacements, since I have no idea how you do it!");
	}

	@Override
	public void animationStep() {
		Enumeration<Figure> k = figures();
		while (k.hasMoreElements()) {
			Figure f = k.nextElement();
			if (f instanceof IAnimatable)
				((IAnimatable) f).animationStep();
		}
	}

	@Override
	public void ceaseAnimation() {
		Enumeration<Figure> k = figures();
		while (k.hasMoreElements()) {
			Figure f = k.nextElement();
			if (f instanceof IAnimatable)
				((IAnimatable) f).ceaseAnimation();
		}
	}

	// Keeps really nice loose coupling BUT current method limits one figure to
	// one decorator.
	// A potential scheme could map to a set of decorators and then additionally
	// provided arguments
	// designate which to select. Overly complex for this instance where I only
	// need 1->1 mapping.
	protected IDecoratorFactory getFactory() {
		return DecoratorFactory.getInstance();
	}
}
