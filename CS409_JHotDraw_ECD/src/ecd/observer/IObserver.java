package ecd.observer;
/**
 * Interface illustrating usage of Observer Pattern.
 * Designed to avoid any casting from occurring through using
 * the default provided implementation.  Instead, the contents
 * of the messages are known through {@link IMessage}.
 */
public interface IObserver<T> {
	/**
	 * Update this with the contents of {@link IMessage}
	 * @param msg the crucial update message
	 */
	void update(IMessage<T> msg);
}
