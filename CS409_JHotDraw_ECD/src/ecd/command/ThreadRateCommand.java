package ecd.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import ecd.observer.IMessage;
import ecd.observer.IObservable;
import ecd.observer.IObserver;
import ecd.observer.ThreadRateMessage;
import CH.ifa.draw.command.Command;

/**
 * Small implementation of the Observer pattern. Simply notifies observers in
 * the case the command is executed with valid inputs.
 * 
 * @author Sean
 */
public class ThreadRateCommand extends Command implements IObservable<Integer> {

	private final Collection<IObserver<Integer>> observers;
	private final AtomicBoolean lock;

	public ThreadRateCommand(String name, AtomicBoolean lock) {
		super(name);
		this.observers = new ArrayList<>();
		this.lock = lock;
	}

	@Override
	public boolean isExecutable() {
		return !(this.lock.get());
	}

	@Override
	public void execute() {
		if (isExecutable()) {
			final JTextField input = new JTextField();
			final JComponent[] inputs = new JComponent[] {
					new JLabel("Thread Rate:"), input };
			JOptionPane.showMessageDialog(null, inputs, "Dialog",
					JOptionPane.PLAIN_MESSAGE);
			try {
				int temp = Integer.parseInt(input.getText());
				if (temp > 0) {
					this.notifyObservers(new ThreadRateMessage(temp));
				}
			} catch (NumberFormatException e) {
			}
		}
	}

	@Override
	public void addObserver(IObserver<Integer> obs) {
		this.observers.add(obs);
	}

	@Override
	public void removeObserver(IObserver<Integer> obs) {
		this.observers.remove(obs);
	}

	@Override
	public void notifyObservers(IMessage<Integer> msg) {
		for (IObserver<Integer> obs : this.observers)
			obs.update(msg);
	}

}
