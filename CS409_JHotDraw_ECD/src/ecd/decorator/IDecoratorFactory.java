package ecd.decorator;

import java.lang.reflect.InvocationTargetException;

import ecd.figures.IElectricalFigure;

/**
 * Implementation of the Factory pattern for manufacturing different types of
 * Decorators for IElectricalFigures.
 * 
 * Each Decorator is responsible for registering the class it can decorate and
 * itself with the factory.
 * 
 * @author Sean
 */
public interface IDecoratorFactory {
	/**
	 * Register the decorated object with class item with the decorator of class
	 * decorator
	 * 
	 * @param item
	 *            : The registered class of the item
	 * @param decorator
	 *            : The class of the decorator
	 */
	void registerDecorator(Class<? extends IElectricalFigure> item,
			Class<? extends IElectricalFigure> decorator);

	/**
	 * Request an IElectricalFigure which is capable of decorating the class of
	 * item
	 * 
	 * @param item
	 *            : The registered class of the item
	 * @return the linked IElectricalFigure acting as decorator
	 * @throws InstantiationException
	 *             : No empty parameter constructor
	 * @throws IllegalAccessException
	 *             : Reflection issue encountered
	 */
	IElectricalFigure getDecorator(Class<? extends IElectricalFigure> item)
			throws InstantiationException, IllegalAccessException;

	/**
	 * Request an IElectricalFigure which is capable of decorating the class of
	 * item which accepts params in a publically available constructor.
	 * 
	 * @param item
	 *            : The registered class of the item
	 * @param params
	 *            : Parameters to be supplied to the decorator constructor
	 * @return the linked IElectricalFigure acting as decorator
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	IElectricalFigure getDecorator(Class<? extends IElectricalFigure> item,
			Object... params) throws InstantiationException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException;
}
