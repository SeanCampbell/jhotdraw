package ecd.strategies;

import ecd.figures.IElectricalFigure.LogicLevel;

/**
 * Default, abstract implementation. Contractually ensures extensions will not
 * receive invalid input via the template pattern.
 * 
 * @author Sean
 *
 */
public abstract class Strategy implements IStrategy {

	private static final long serialVersionUID = -3673343324450632368L;

	/**
	 * Strategy pattern in conjunction with template. First ensures none of the
	 * input {@link LogicLevel}s are undefined, in which case the return value
	 * is automatically undefined. Next, it calls the strategy variable method
	 * if no inputs were undefined and returns the relevant {@link LogicLevel}.
	 */
	@Override
	public final LogicLevel execute(LogicLevel[] inputs) {
		LogicLevel ret = LogicLevel.LOW;
		for (LogicLevel l : inputs) {
			if (l.equals(LogicLevel.UNDEFINED)) {
				ret = LogicLevel.UNDEFINED;
				break;
			}
		}
		if (ret.equals(LogicLevel.LOW))
			ret = getStrategyOutput(inputs);
		return ret;
	}

	/**
	 * The method each strategy is responsible for overriding, in the event the
	 * template considers the inputs acceptable for the strategy.
	 * 
	 * @param inputs
	 *            : LogicLevel array; solely LogicLevel.HIGH or LogicLevel.LOW
	 * @return the output for the strategy
	 */
	protected abstract LogicLevel getStrategyOutput(LogicLevel[] inputs);

	// By default the number of inputs can be altered for each strategy
	@Override
	public boolean inputCountAlterable() {
		return true;
	}

}
