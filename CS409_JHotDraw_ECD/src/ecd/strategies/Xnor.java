package ecd.strategies;

import ecd.figures.IElectricalFigure.LogicLevel;

public class Xnor extends Xor {

	private static final long serialVersionUID = -8957208762840547555L;

	private static final String NAME = "XNOR";
	private static final Integer DEF_INPUT_PINS = 2;

	static {
		StrategyFactory.getInstance().registerStrategy(Xnor.NAME, Xnor.class);
		StrategyFactory.getInstance().registerInput(Xnor.NAME,
				Xnor.DEF_INPUT_PINS);
	}

	@Override
	public String strategyName() {
		return Xnor.NAME;
	}

	@Override
	protected LogicLevel getStrategyOutput(LogicLevel[] inputs) {
		LogicLevel ret = super.getStrategyOutput(inputs);
		if (inputs.length % 2 == 0) {
			ret = LogicLevel.toggle(ret);
		}
		return ret;
	}

}
