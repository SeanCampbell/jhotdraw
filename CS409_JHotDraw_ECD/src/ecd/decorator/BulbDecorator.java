package ecd.decorator;

import java.awt.Color;
import java.io.IOException;
import java.util.Iterator;

import CH.ifa.draw.storable.StorableInput;
import ecd.figures.BulbFigure;
import ecd.figures.ElectricalFigure;
import ecd.figures.IElectricalFigure;

/**
 * Decorator for {@link BulbFigure}. Attached animation responsibilities cause
 * the bulb to change colour in the event of a change in {@link LogicLevel}.
 * 
 * @author Sean
 */
public class BulbDecorator extends ElectricalDecorator {

	private static final long serialVersionUID = 1585329673992684913L;

	private BulbFigure bulb;

	static {
		DecoratorFactory.getInstance().registerDecorator(BulbFigure.class,
				BulbDecorator.class);
	}

	public BulbDecorator(BulbFigure fig) {
		super(fig);
		this.bulb = fig;
	}

	public BulbDecorator() {
		super();
	}

	@Override
	public void animationStep() {
		Iterator<IElectricalFigure> iter = this.bulb.getPredecessors()
				.iterator();
		// In case of no predecessor, update forces action to off-state
		if (iter.hasNext()) {
			LogicLevel power = iter.next().getOutput();
			if (power.equals(LogicLevel.HIGH)) {
				super.setAttribute(ElectricalFigure.FILL_ATTRIB,
						getOnColorProperty());
			} else {
				super.setAttribute(ElectricalFigure.FILL_ATTRIB,
						BulbFigure.DEF_OFF_COLOR);
			}
		} else {
			super.setAttribute(ElectricalFigure.FILL_ATTRIB,
					BulbFigure.DEF_OFF_COLOR);
		}
		return;
	}

	// Nasty way of building the colour based upon user input.
	private Color getOnColorProperty() {
		String name = super.getProperties().get(BulbFigure.ON_COLOR)
				.toLowerCase();
		Color ret = BulbFigure.DEF_ON_COLOR;
		if (!name.equalsIgnoreCase(BulbFigure.ON_COLOR_)) {
			// Attempt to build the colour by name using reflection
			try {
				java.lang.reflect.Field field = Class.forName("java.awt.Color")
						.getField(name);
				ret = (Color) field.get(null);
			} catch (Exception e) {// Evidently an invalid color; how to
									// propogate up?
			}
		}
		return ret;
	}

	@Override
	public void ceaseAnimation() {
		super.setAttribute(ElectricalFigure.FILL_ATTRIB,
				BulbFigure.DEF_OFF_COLOR);
		return;
	}

	@Override
	public void read(StorableInput dr) throws IOException {
		super.read(dr);
		this.bulb = (BulbFigure) fComponent;
	}
}
