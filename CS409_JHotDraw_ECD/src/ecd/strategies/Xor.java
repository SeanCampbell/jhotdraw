package ecd.strategies;

import ecd.figures.IElectricalFigure.LogicLevel;

public class Xor extends Strategy {

	private static final long serialVersionUID = 4969285938895740867L;

	private static final String NAME = "XOR";
	private static final Integer DEF_INPUT_PINS = 2;

	static {
		StrategyFactory.getInstance().registerStrategy(Xor.NAME, Xor.class);
		StrategyFactory.getInstance().registerInput(Xor.NAME,
				Xor.DEF_INPUT_PINS);
	}

	@Override
	public String strategyName() {
		return Xor.NAME;
	}

	@Override
	protected LogicLevel getStrategyOutput(LogicLevel[] inputs) {
		int trueCount = 0;
		for (LogicLevel l : inputs) {
			if (l.equals(LogicLevel.HIGH)) {
				trueCount++;
			}
		}
		return (trueCount % 2 == 1) ? LogicLevel.HIGH : LogicLevel.LOW;
	}

}
