package ecd.figures;

import java.awt.Color;
import java.awt.Point;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import ecd.strategies.IStrategy;
import ecd.strategies.StrategyFactory;
import CH.ifa.draw.figure.RectangleFigure;
import CH.ifa.draw.figure.TextFigure;
import CH.ifa.draw.framework.Figure;
import CH.ifa.draw.storable.StorableInput;
import CH.ifa.draw.storable.StorableOutput;

/**
 * A typical figure signifying a logic gate. The output the logic gate generates
 * is delegated to a specific instance of {@link IStrategy} and the gate is
 * named by retrieving the strategy name.
 * 
 * @author Sean
 */
public class LogicGateFigure extends ElectricalFigure {

	private static final long serialVersionUID = 4717554084037261916L;

	private static final String TEXT_COLOR = "TextColor",
			TEXT_SIZE = "FontSize";

	private static final String INPUT_COUNT = "Number of Inputs:";

	private static final Color GATE_COLOR = Color.BLACK,
			TEXT_COLOR_ = Color.WHITE;

	private static final int BOX_L = 34, TEXT_SIZE_ = 11;

	private IStrategy strategy;

	private int inputCount;

	public LogicGateFigure(final IStrategy strategy, final int inputCount) {
		super();
		this.strategy = strategy;
		this.inputCount = inputCount;
		super.setAttribute(TEXT_ATTRIB, strategy.strategyName());
		super.setProperty(INPUT_COUNT, Integer.toString(inputCount));
	}

	// Loading from non-serialised field uses reflection with empty constructor
	public LogicGateFigure() {
		super();
	}

	protected Collection<Figure> getGeneralLook() {
		final List<Figure> figures = new Vector<>();
		figures.add(new RectangleFigure(new Point(0, 0),
				new Point(BOX_L, BOX_L)));
		figures.get(0).setAttribute(ElectricalFigure.FILL_ATTRIB, GATE_COLOR);
		final TextFigure tf = new TextFigure();
		tf.setAttribute(TEXT_SIZE, TEXT_SIZE_);
		tf.setReadOnly(true);
		tf.setAttribute(TEXT_COLOR, TEXT_COLOR_);
		figures.add(tf);
		return figures;
	}

	@Override
	public boolean canConnectInput() {
		return this.inputCount != super.getPredecessors().size();
	}

	@Override
	public LogicLevel getOutput() {
		final LogicLevel[] inputs = this.getInputs();
		LogicLevel output = LogicLevel.UNDEFINED;
		if (inputs.length == this.inputCount) {
			output = strategy.execute(inputs);
		}
		return output;
	}

	private LogicLevel[] getInputs() {
		Collection<IElectricalFigure> preds = super.getPredecessors();

		int i = 0;
		LogicLevel[] inputs = new LogicLevel[preds.size()];
		for (Iterator<IElectricalFigure> iter = preds.iterator(); iter
				.hasNext();) {
			inputs[i++] = iter.next().getOutput();
		}
		return inputs;
	}

	@Override
	public void read(StorableInput dr) throws IOException {
		super.read(dr);
		try {
			this.strategy = StrategyFactory.getInstance().getStrategy(
					dr.readString());
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		this.inputCount = dr.readInt();
	}

	@Override
	public void write(StorableOutput dw) {
		super.write(dw);
		dw.writeString(this.strategy.strategyName());
		dw.writeInt(this.inputCount);
	}

	@Override
	public void setProperty(String name, String value) {
		if (name.equals(INPUT_COUNT)) {
			try {
				int inputCount = Integer.parseInt(value);
				// Would be nice if I could somehow notify someone if one of
				// these checks failed....
				if (this.strategy.inputCountAlterable() && inputCount > 0
						&& inputCount >= this.getPredecessors().size()) {
					this.inputCount = inputCount;
				}
			} catch (NumberFormatException e) {
			}
		} else
			super.setProperty(name, value);
	}
}
