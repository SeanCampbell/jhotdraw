package ecd.command;

import java.util.Iterator;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import ecd.figures.IElectricalFigure;
import CH.ifa.draw.command.Command;
import CH.ifa.draw.framework.DrawingView;
import CH.ifa.draw.framework.Figure;

/**
 * Unused. Initial idea was to employ the frame-work command features. Dropped
 * in favour of exploiting a feature of the select tool where properties are
 * instead altered by double clicking on any figure. Only disadvantage is that
 * the select tool cannot work on multiple figures whereas the command method
 * could, if the figures were of the same type.
 */
public class EditSelectedCommand extends Command {

	private DrawingView view;

	public EditSelectedCommand(String name, DrawingView view) {
		super(name);
		this.view = view;
	}

	@Override
	public boolean isExecutable() {
		return this.view.selectionCount() == 1
				&& this.view.selection().get(0) instanceof IElectricalFigure;
	}

	@Override
	public void execute() {
		// Do the check. isExecutable() is not called before Figures are
		// actually added.
		// This is a flaw which exists with the allignment and edit menu bars as
		// well.
		if (this.view.selectionCount() == 1) {
			Figure fig = this.view.selection().get(0);
			if (fig instanceof IElectricalFigure) {
				final IElectricalFigure elecFig = (IElectricalFigure) fig;
				final Map<String, String> properties = elecFig.getProperties();
				if (properties.size() != 0) {
					final JTextField[] fields = this.obtainInput(properties);
					final Iterator<String> iter = properties.keySet()
							.iterator();
					for (int i = 0; i != properties.size(); i++)
						elecFig.setProperty(iter.next(), fields[i].getText());
				}
			}
		}
	}

	private JTextField[] obtainInput(final Map<String, String> properties) {
		final JComponent[] inputs = new JComponent[properties.size() * 2];
		final JTextField[] fields = new JTextField[properties.size()];
		Iterator<String> iter = properties.keySet().iterator();
		for (int i = 0, j = 0, len = properties.size() * 2; i != len; i += 2, j = i / 2) {
			fields[j] = new JTextField();
			inputs[i] = new JLabel(iter.next());
			inputs[i + 1] = fields[j];
		}
		JOptionPane.showMessageDialog(null, inputs, "Dialog",
				JOptionPane.PLAIN_MESSAGE);
		return fields;
	}
}
