package ecd.app;

import java.util.concurrent.atomic.AtomicBoolean;

import CH.ifa.draw.framework.DrawingView;

/**
 * Implementation of runnable. Employs a flag to allow reliable halting of the
 * animation and any clean-up to be performed.
 * 
 * @author Sean
 */
public class Animation implements Runnable {
	private final DrawingView fView;
	private final IAnimatable fAnimatable;
	// A flag which can be altered by another thread; used to signify
	// whether this thread should stop or continue.
	private final AtomicBoolean lock;
	// The frequency of operation for the thread.
	private final int rate;

	/**
	 * Construct an instance of Animation.
	 * 
	 * @param fView
	 *            : The view of the application
	 * @param fAnimatable
	 *            : The Animtable object
	 * @param lock
	 *            : A lock/flag to check before performing an operation. The
	 *            animation continues in the event the flag evaluates to true
	 *            while it cleans up and finishes in the evant of a false
	 *            evaluation.
	 * @param rate
	 *            : The sleep time before performing checking the flag and
	 *            performing subsequent animation or clean-up.
	 */
	public Animation(DrawingView fView, IAnimatable fAnimatable,
			AtomicBoolean lock, int rate) {
		this.fView = fView;
		this.fAnimatable = fAnimatable;
		this.lock = lock;
		this.rate = rate;
	}

	@Override
	public void run() {
		while (this.lock.get()) {
			try {
				Thread.sleep(this.rate);
			} catch (InterruptedException e) {
			}
			this.fView.freezeView();
			if (this.lock.get())
				this.fAnimatable.animationStep();
			else
				this.fAnimatable.ceaseAnimation();
			this.fView.checkDamage();
			this.fView.unfreezeView();
		}
	}

}
