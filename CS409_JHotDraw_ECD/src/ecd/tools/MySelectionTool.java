/*
 * @(#)MySelectionTool.java 5.1
 *
 */

package ecd.tools;

import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import ecd.figures.IElectricalFigure;
import CH.ifa.draw.framework.DrawingView;
import CH.ifa.draw.framework.Figure;
import CH.ifa.draw.tool.SelectionTool;

/**
 * A SelectionTool that interprets double clicks to alter changeable properties
 * of a figure, if it has any.
 * 
 * @author Sean
 */
public class MySelectionTool extends SelectionTool {

	public MySelectionTool(DrawingView view) {
		super(view);
	}

	private void alterFigure(IElectricalFigure elecFig) {
		final Map<String, String> properties = elecFig.getProperties();
		if (properties.size() != 0) {
			final JTextField[] fields = this.obtainInput(properties);
			final Iterator<String> iter = properties.keySet().iterator();
			for (int i = 0; i != properties.size(); i++)
				elecFig.setProperty(iter.next(), fields[i].getText());
		}
	}

	private JTextField[] obtainInput(final Map<String, String> properties) {
		final JComponent[] inputs = new JComponent[properties.size() * 2];
		final JTextField[] fields = new JTextField[properties.size()];
		Iterator<String> iter = properties.keySet().iterator();
		for (int i = 0, j = 0, len = properties.size() * 2; i != len; i += 2, j = i / 2) {
			fields[j] = new JTextField();
			inputs[i] = new JLabel(iter.next());
			inputs[i + 1] = fields[j];
		}
		JOptionPane.showMessageDialog(null, inputs, "Dialog",
				JOptionPane.PLAIN_MESSAGE);
		return fields;
	}

	/**
	 * Handles mouse down events and starts the corresponding tracker.
	 */
	@Override
	public void mouseDown(MouseEvent e, int x, int y) {
		if (e.getClickCount() == 2 && view().selectionCount() == 1) {
			Figure figure = drawing().findFigure(e.getX(), e.getY());
			if (figure != null && figure instanceof IElectricalFigure) {
				alterFigure((IElectricalFigure) figure);
				return;
			}
		}
		super.mouseDown(e, x, y);
	}

}
