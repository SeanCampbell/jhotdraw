package ecd.strategies;

import java.util.Map;

/**
 * Implementation of the Factory pattern for manufacturing different types of
 * Strategies usable for logic gates.
 * 
 * Each Strategy is responsible for registering itself and its default number of
 * inputs with the factory to allow manufacture.
 * 
 * @author Sean
 */
public interface IStrategyFactory {
	/**
	 * Request an IStrategy which has been registered by the name strategy.
	 * 
	 * @param strategy
	 *            : The registered name for the strategy
	 * @return the linked IStrategy
	 * @throws InstantiationException
	 *             : No empty parameter constructor
	 * @throws IllegalAccessException
	 *             : Reflection issue encountered
	 */
	IStrategy getStrategy(String strategy) throws InstantiationException,
			IllegalAccessException;

	/**
	 * Register the relevant implementation of IStrategy clazz with the given
	 * name strategy.
	 * 
	 * @param strategy
	 *            : The registered name for the strategy
	 * @param clazz
	 *            : The IStrategy to be linked
	 */
	void registerStrategy(String strategy, Class<? extends IStrategy> clazz);

	/**
	 * Register the relevant default number of inputs with the given name
	 * strategy.
	 * 
	 * @param strategy
	 *            : The registered name for the default number of inputs
	 * @param numInputs
	 *            : The default number of inputs
	 */
	void registerInput(String strategy, Integer numInputs);

	/**
	 * Fetch the scheme mapping the names of strategies to the relevant
	 * implementations of IStrategy
	 * 
	 * @return the mapping scheme
	 */
	Map<String, Class<? extends IStrategy>> getStrategies();

	/**
	 * Fetch the scheme mapping the names of strategies to the relevant default
	 * number of inputs
	 * 
	 * @return the mapping scheme
	 */
	Map<String, Integer> getDefaultInputs();
}
