package ecd.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import ecd.command.ThreadRateCommand;
import ecd.figures.BatteryFigure;
import ecd.figures.BulbFigure;
import ecd.figures.LogicGateFigure;
import ecd.figures.PulseSourceFigure;
import ecd.figures.SpeakerFigure;
import ecd.figures.Wire;
import ecd.observer.IMessage;
import ecd.observer.IObserver;
import ecd.strategies.IStrategy;
import ecd.strategies.IStrategyFactory;
import ecd.strategies.StrategyFactory;
import ecd.tools.CustomCreationTool;
import ecd.tools.MySelectionTool;
import CH.ifa.draw.application.DrawApplication;
import CH.ifa.draw.command.Command;
import CH.ifa.draw.command.CommandMenu;
import CH.ifa.draw.framework.Drawing;
import CH.ifa.draw.framework.Tool;
import CH.ifa.draw.tool.ConnectionTool;

/**
 * The main application.
 * 
 * @author Sean
 */
public class ECDApplication extends DrawApplication implements
		IObserver<Integer> {

	private static final long serialVersionUID = -3658994283358123545L;
	private static final String IMAGE_PATH = "/ecd/images/";
	private static final int DEF_THREAD_RATE = 1000 / 20;

	// Static threadpool for all instances
	private static ExecutorService animationPool;
	// Personal lock; used to signal the animation thread: True =
	// start/continue, False = stop
	private AtomicBoolean lock;
	private int threadRate;

	// Since many windows can be spawned, an equivalent number of animation
	// threads is needed.
	// Statically allocating is more efficient.
	static {
		animationPool = Executors.newSingleThreadExecutor();
	}

	public static void main(String[] args) {
		ECDApplication app = new ECDApplication();
		app.open();
	}

	public ECDApplication() {
		super("ECD Application");
		this.lock = new AtomicBoolean(false);
		this.threadRate = DEF_THREAD_RATE;
		// Want to avoid memory leakage. If user clicks to close the program in
		// the
		// middle of simulation, we ensure the animation thread finishes
		// gracefully.
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				lock.getAndSet(false);
			}
		});
	}

	@Override
	protected void createTools(JPanel panel) {
		super.createTools(panel);

		Tool tool;
		tool = new CustomCreationTool(view(), new BatteryFigure());
		panel.add(createToolButton(ECDApplication.IMAGE_PATH + "source",
				"Source", tool));

		tool = new CustomCreationTool(view(), new PulseSourceFigure());
		panel.add(createToolButton(ECDApplication.IMAGE_PATH + "source",
				"Pulse Source", tool));

		// Get the strategy factory and make logic gates with each strategy
		IStrategyFactory factory = this.getFactory();
		Map<String, Class<? extends IStrategy>> classMap = factory
				.getStrategies();
		Map<String, Integer> integerMap = factory.getDefaultInputs();
		try {
			for (Map.Entry<String, Class<? extends IStrategy>> entry : classMap
					.entrySet()) {
				tool = new CustomCreationTool(view(), new LogicGateFigure(
						factory.getStrategy(entry.getKey()),
						integerMap.get(entry.getKey())));
				panel.add(createToolButton(
						ECDApplication.IMAGE_PATH + entry.getKey(),
						entry.getKey() + " Gate", tool));
			}
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			System.exit(-1);
		}

		tool = new CustomCreationTool(view(), new BulbFigure());
		panel.add(createToolButton(ECDApplication.IMAGE_PATH + "bulb", "Bulb",
				tool));

		tool = new CustomCreationTool(view(), new SpeakerFigure());
		panel.add(createToolButton(ECDApplication.IMAGE_PATH + "speaker",
				"Speaker", tool));

		tool = new ConnectionTool(view(), new Wire());
		panel.add(createToolButton(ECDApplication.IMAGE_PATH + "CONN", "Wire", tool));
	}

	@Override
	protected Tool createSelectionTool() {
		return new MySelectionTool(view());
	}

	@Override
	protected void createMenus(JMenuBar mb) {
		super.createMenus(mb);
		mb.add(createCircuitMenu());
		mb.add(createMiscMenu());
	}

	@Override
	protected Drawing createDrawing() {
		return new ElectricalDrawing();
	}

	// Allows sub-classing to add LogicGateFigures without overriding
	// createTools()
	protected IStrategyFactory getFactory() {
		return StrategyFactory.getInstance();
	}

	private JMenu createCircuitMenu() {
		CommandMenu menu = new CommandMenu("Circuit");
		JMenuItem mi = new JMenuItem("Start Simulation");
		mi.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				startSimulation();
			}
		});
		menu.add(mi);

		mi = new JMenuItem("End Simulation");
		mi.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				endSimulation();
			}
		});
		menu.add(mi);
		return menu;
	}

	private JMenu createMiscMenu() {
		CommandMenu menu = new CommandMenu("Misc");
		ThreadRateCommand trc = new ThreadRateCommand("Change Thread Rate",
				this.lock);
		trc.addObserver(this);
		menu.add((Command) trc);
		return menu;
	}

	protected void startSimulation() {
		if (drawing() instanceof IAnimatable && !this.lock.get()) {
			this.lock.getAndSet(true);
			animationPool.submit(this.getAnimationRunnable());
		}
	}

	protected int getThreadRate() {
		return this.threadRate;
	}

	protected void endSimulation() {
		if (this.lock.get()) {
			this.lock.getAndSet(false);
		}
	}

	// Allows sub-class overriding of the runnable injected
	protected Runnable getAnimationRunnable() {
		return new Animation(view(), (IAnimatable) drawing(), this.lock,
				getThreadRate());
	}

	@Override
	public void update(IMessage<Integer> msg) {
		this.threadRate = msg.getMessage();
	}
}
