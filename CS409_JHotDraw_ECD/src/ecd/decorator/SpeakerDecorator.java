package ecd.decorator;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import CH.ifa.draw.storable.StorableInput;
import ecd.figures.IElectricalFigure;
import ecd.figures.SpeakerFigure;

/**
 * Decorator for {@link SpeakerFigure}. Attached animation responsibilities
 * cause the speaker to begin playing a song in the event of a
 * {@link LogicLevel}.HIGH on the input. A default is set though the user can
 * alter the song played.
 * 
 * @author Sean
 */
public class SpeakerDecorator extends ElectricalDecorator {

	private static final long serialVersionUID = 1585329673992684913L;
	private static final String SOUND_PATH = "ecd/sound/", SONG = "Song Name:",
			DEFAULT_SONG = "CountingStars.wav", WAV = ".wav", PCM = ".pcm";

	// Shared thread-pool; beats single threads amongst instances
	private static final transient ExecutorService threadPool;
	// Reference to the current song effectively
	private transient Future<?> playable;
	// Interestingly checking Thread.interrupted() never seemed to work.
	// Provided my own volatile flag to check after every buffer read; atomicity
	// not required
	private volatile transient boolean flag;

	private SpeakerFigure speaker;

	static {
		threadPool = Executors.newCachedThreadPool();
		DecoratorFactory.getInstance().registerDecorator(SpeakerFigure.class,
				SpeakerDecorator.class);
	}

	public SpeakerDecorator(SpeakerFigure fig) {
		super(fig);
		this.speaker = fig;
		this.playable = null;
		this.flag = false;
		this.speaker.setProperty(SONG, DEFAULT_SONG);
	}

	public SpeakerDecorator() {
		super();
	}

	@Override
	public void animationStep() {
		Iterator<IElectricalFigure> iter = this.getPredecessors().iterator();
		if (iter.hasNext()) {
			LogicLevel power = iter.next().getOutput();
			if (power.equals(LogicLevel.HIGH)) {
				this.flag = false;
				if (this.playable == null)
					this.playable = SpeakerDecorator.threadPool
							.submit(new Playable());
			} else {
				this.flag = true;
				this.playable = null;
			}
		} else {
			this.flag = true;
			this.playable = null;
		}
		return;
	}

	@Override
	public void ceaseAnimation() {
		this.flag = true;
		this.playable = null;
		return;
	}

	@Override
	public void read(StorableInput dr) throws IOException {
		super.read(dr);
		this.speaker = (SpeakerFigure) fComponent;
	}

	// Custom audio-stream
	private class Playable implements Runnable {
		private javax.sound.sampled.AudioInputStream is;
		private javax.sound.sampled.AudioFormat format;
		private javax.sound.sampled.SourceDataLine line;

		private byte[] buffer;

		public Playable() {
			try {
				String item = getProperties().get(SONG);
				item = (item.endsWith(WAV) | item.endsWith(PCM)) ? SOUND_PATH
						+ item : SOUND_PATH + DEFAULT_SONG;
				java.net.URL soundURL = SpeakerFigure.class.getClassLoader()
						.getResource(item);
				java.io.File soundFile;
				if (soundURL != null) {
					soundFile = new java.io.File(soundURL.getPath());
				} else {
					soundFile = new java.io.File(SpeakerFigure.class
							.getClassLoader()
							.getResource(SOUND_PATH + DEFAULT_SONG).getPath());
				}
				is = javax.sound.sampled.AudioSystem
						.getAudioInputStream(soundFile);
				format = is.getFormat();
				javax.sound.sampled.DataLine.Info info = new javax.sound.sampled.DataLine.Info(
						javax.sound.sampled.SourceDataLine.class, format);
				line = (javax.sound.sampled.SourceDataLine) javax.sound.sampled.AudioSystem
						.getLine(info);
				buffer = new byte[10000];
			} catch (javax.sound.sampled.UnsupportedAudioFileException
					| java.io.IOException
					| javax.sound.sampled.LineUnavailableException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			try {
				line.open(format);
				line.start();
				int count = 0;
				while ((count = is.read(buffer, 0, buffer.length)) != -1
						&& !flag) {
					if (count > 0) {
						line.write(buffer, 0, count);
					}
				}
				line.drain();
				line.close();
			} catch (javax.sound.sampled.LineUnavailableException
					| java.io.IOException e) {
				e.printStackTrace();
			} finally {
				try {
					is.close();
				} catch (java.io.IOException e) { // Swallow. Bad?
				}
			}
		}
	}
}
