package ecd.strategies;

import java.io.Serializable;

import ecd.figures.IElectricalFigure.LogicLevel;

/**
 * Implementation of the Strategy pattern. Each strategy is capable of executing
 * based on a number of inputs, returning its name and returning whether the
 * number of inputs acceptable is alterable.
 * 
 * @author Sean
 */
public interface IStrategy extends Serializable {
	/**
	 * Execute the strategy for the given inputs.
	 * 
	 * @param inputs
	 *            : LogicLevel array necessary to obtain the output
	 * @return the relevant output
	 */
	LogicLevel execute(LogicLevel[] inputs);

	/**
	 * Check whether the number of inputs for the strategy is alterable.
	 * 
	 * @return true if alterable, false otherwise
	 */
	boolean inputCountAlterable();

	/**
	 * Obtain the name for the strategy
	 * 
	 * @return a string representation of the name
	 */
	String strategyName();
}
