package ecd.strategies;

import ecd.figures.IElectricalFigure.LogicLevel;

public class Nor extends Or {

	private static final long serialVersionUID = -8523787036376046425L;

	private static final String NAME = "NOR";
	private static final Integer DEF_INPUT_PINS = 2;

	static {
		StrategyFactory.getInstance().registerStrategy(Nor.NAME, Nor.class);
		StrategyFactory.getInstance().registerInput(Nor.NAME,
				Nor.DEF_INPUT_PINS);
	}

	@Override
	public LogicLevel getStrategyOutput(LogicLevel[] inputs) {
		return LogicLevel.toggle(super.getStrategyOutput(inputs));
	}

	@Override
	public String strategyName() {
		return Nor.NAME;
	}

}
