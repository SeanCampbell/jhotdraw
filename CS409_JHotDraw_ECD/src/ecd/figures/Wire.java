package ecd.figures;

import java.util.Vector;

import CH.ifa.draw.figure.ArrowTip;
import CH.ifa.draw.figure.PolyLineFigure;
import CH.ifa.draw.figure.connection.LineConnection;
import CH.ifa.draw.framework.Figure;
import CH.ifa.draw.framework.Handle;
import CH.ifa.draw.handle.NullHandle;

/**
 * Effectively the means by which {@link IElectricalFigure}s can be connected
 * together.
 * 
 * @author Sean
 */
public class Wire extends LineConnection {

	private static final long serialVersionUID = -8661630274349737831L;

	public Wire() {
		setEndDecoration(new ArrowTip());
		setStartDecoration(null);
	}

	@Override
	public boolean canConnect(Figure start, Figure end) {
		boolean ret = false;
		if (start instanceof IElectricalFigure
				&& end instanceof IElectricalFigure) {
			IElectricalFigure source = (IElectricalFigure) start;
			IElectricalFigure target = (IElectricalFigure) end;
			if (!source.hasCycle(target))
				ret = source.canConnectOutput() && target.canConnectInput();
		}
		return ret;
	}

	@Override
	public void handleConnect(Figure start, Figure end) {
		IElectricalFigure source = (IElectricalFigure) start;
		IElectricalFigure target = (IElectricalFigure) end;
		if (!source.equals(target)) {
			source.addSuccessor(target);
			target.addPredecessor(source);
		}
	}

	@Override
	public void handleDisconnect(Figure start, Figure end) {
		IElectricalFigure source = (IElectricalFigure) start;
		IElectricalFigure target = (IElectricalFigure) end;
		if (source != null) {
			source.removeSuccessor(target);
		}
		if (target != null) {
			target.removePredecessor(source);
		}
	}

	@Override
	public Vector<Handle> handles() {
		Vector<Handle> handles = super.handles();
		// Prevent re-connections; simply show connected figures
		handles.setElementAt(new NullHandle(this, PolyLineFigure.locator(0)), 0);
		return handles;
	}

}
