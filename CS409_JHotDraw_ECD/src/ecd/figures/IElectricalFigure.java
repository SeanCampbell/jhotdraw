package ecd.figures;

import java.util.Collection;
import java.util.Map;

import CH.ifa.draw.framework.Figure;

/**
 * Primary interface which all electrical components and their decorators
 * implements.
 * 
 * Electrical components have predecessors which act as inputs and successors
 * which receive input from this. They may be constrained such that they have a
 * limited number of inputs or outputs and additionally may have a list of
 * editable properties which the user is capable of setting.
 * 
 * @author Sean
 */
public interface IElectricalFigure extends Figure {
	/**
	 * Logic level enum to establish clarity in instances such as floating ports
	 * on components. Naturally, these will be undefined in terms of logic
	 * level.
	 * 
	 * @author Sean
	 */
	public enum LogicLevel {
		LOW(0), UNDEFINED(1), HIGH(2);
		private int value;

		private LogicLevel(int value) {
			this.value = value;
		}

		public int getValue() {
			return this.value;
		}

		public static LogicLevel toggle(LogicLevel level) {
			LogicLevel ret = LogicLevel.UNDEFINED;
			if (level.equals(LogicLevel.HIGH))
				ret = LogicLevel.LOW;
			else if (level.equals(LogicLevel.LOW))
				ret = LogicLevel.HIGH;
			return ret;
		}
	}

	/**
	 * Fetch the IElectricalFigures acting as inputs.
	 * 
	 * @return Collection of IElectricalFigures
	 */
	Collection<IElectricalFigure> getPredecessors();

	/**
	 * Fetch the IElectricalFigures this outputs to.
	 * 
	 * @return Collection of IElectricalFigures
	 */
	Collection<IElectricalFigure> getSuccessors();

	/**
	 * Setup pred as an input for this.
	 * 
	 * @param pred
	 *            : The IElectricalFigure acting as input
	 */
	void addPredecessor(IElectricalFigure pred);

	/**
	 * Setup succ as an output for this.
	 * 
	 * @param succ
	 *            : The IElectricalFigure acting as output
	 */
	void addSuccessor(IElectricalFigure succ);

	/**
	 * Remove pred as an input for this.
	 * 
	 * @param pred
	 *            : The IElectricalFigure acting as input
	 */
	void removePredecessor(IElectricalFigure pred);

	/**
	 * Remove succ as an output for this.
	 * 
	 * @param succ
	 *            : The IElectricalFigure acting as output
	 */
	void removeSuccessor(IElectricalFigure succ);

	/**
	 * Check whether this will accept any(more) inputs
	 * 
	 * @return true if able to, false otherwise
	 */
	boolean canConnectInput();

	/**
	 * Check whether this can be connected as output to any more
	 * IElectricalFigures
	 * 
	 * @return true if able to, false otherwise
	 */
	boolean canConnectOutput();

	/**
	 * Check whether there is a cycle between IElectricalFigures
	 * 
	 * @return true if a cycle exists, false otherwise
	 */
	boolean hasCycle(IElectricalFigure fig);

	/**
	 * Set the output for this
	 * 
	 * @param output
	 *            : LogicLevel with value signifying output status
	 */
	void setOutput(LogicLevel output);

	/**
	 * Fetch the output for this {@link LogicLevel}
	 * 
	 * @return LogicLevel with value signifying output status
	 */
	LogicLevel getOutput();

	/**
	 * Fetch a mapping scheme of changeable properties relevant to the
	 * IElectricalFgure
	 * 
	 * @return the map
	 */
	Map<String, String> getProperties();

	/**
	 * Setup a changeable property for the IElectricalFigure
	 * 
	 * @param name
	 *            : The id of the property
	 * @param property
	 *            : The value the property is to adopt
	 */
	void setProperty(String name, String property);
}
