package ecd.figures;

import java.awt.Color;
import java.awt.Point;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import CH.ifa.draw.figure.EllipseFigure;
import CH.ifa.draw.framework.Figure;

/**
 * A bulb which accepts a single input.
 * 
 * @author Sean
 */
public class BulbFigure extends ElectricalFigure {

	private static final long serialVersionUID = 1615838340862557509L;
	// Build Constants
	private static final int OUTER_DIAM = 32, INNER_OFFSET = 2,
			INPUT_COUNT = 1;
	// Default settings
	public static final Color DEF_OFF_COLOR = new Color(51, 51, 0),
			DEF_ON_COLOR = Color.YELLOW;
	public static final String ON_COLOR = "Bulb Color:", ON_COLOR_ = "yellow";

	public BulbFigure() {
		super();
		super.setProperty(ON_COLOR, "yellow");
	}

	protected Collection<Figure> getGeneralLook() {
		final List<Figure> figures = new Vector<>();
		figures.add(new EllipseFigure(new Point(0, 0), new Point(OUTER_DIAM,
				OUTER_DIAM)));
		figures.get(0)
				.setAttribute(ElectricalFigure.FILL_ATTRIB, DEF_OFF_COLOR);
		figures.add(new EllipseFigure(new Point(INNER_OFFSET, INNER_OFFSET),
				new Point(OUTER_DIAM - INNER_OFFSET, OUTER_DIAM - INNER_OFFSET)));
		figures.get(1)
				.setAttribute(ElectricalFigure.FILL_ATTRIB, DEF_OFF_COLOR);
		return figures;
	}

	@Override
	public void addSuccessor(IElectricalFigure pred) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeSuccessor(IElectricalFigure pred) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean canConnectInput() {
		return INPUT_COUNT != super.getPredecessors().size();
	}

	@Override
	public boolean canConnectOutput() {
		return false;
	}
}
