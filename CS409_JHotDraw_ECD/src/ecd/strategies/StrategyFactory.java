package ecd.strategies;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class StrategyFactory implements IStrategyFactory {

	private static final String PACKAGE_NAME = "ecd.strategies.";

	private static IStrategyFactory instance;
	private static final Map<String, Class<? extends IStrategy>> registeredStrategies;
	private static final Map<String, Integer> registeredInputs;

	// Ensure the relevant classes have been loaded; this in turn ensures
	// they have registered themselves with the factory
	static {
		registeredStrategies = new HashMap<>();
		registeredInputs = new HashMap<>();
		try {
			Class.forName(PACKAGE_NAME + "Not");
			Class.forName(PACKAGE_NAME + "And");
			Class.forName(PACKAGE_NAME + "Or");
			Class.forName(PACKAGE_NAME + "Nand");
			Class.forName(PACKAGE_NAME + "Nor");
			Class.forName(PACKAGE_NAME + "Xor");
			Class.forName(PACKAGE_NAME + "Xnor");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	/**
	 * Singleton with lazy initialisation(?) If private constructor was employed
	 * to enforce the singleton, it would prevent sub-classing from occurring.
	 * 
	 * @return the StrategyFactor instance
	 */
	public static IStrategyFactory getInstance() {
		if (instance == null) {
			instance = new StrategyFactory();
		}
		return instance;
	}

	protected StrategyFactory() {
	}

	public IStrategy getStrategy(String strategy)
			throws InstantiationException, IllegalAccessException {
		if (!registeredStrategies.containsKey(strategy))
			throw new InstantiationException("Unknown strategy:\t" + strategy);
		return registeredStrategies.get(strategy).newInstance();
	}

	@Override
	public void registerInput(String strategy, Integer numInputs) {
		registeredInputs.put(strategy, numInputs);
	}

	@Override
	public void registerStrategy(String strategy,
			Class<? extends IStrategy> clazz) {
		registeredStrategies.put(strategy, clazz);
	}

	@Override
	public Map<String, Class<? extends IStrategy>> getStrategies() {
		// Return alphabetised sorted map
		return new TreeMap<>(registeredStrategies);
	}

	@Override
	public Map<String, Integer> getDefaultInputs() {
		return new HashMap<>(registeredInputs);
	}
}
