package ecd.strategies;

import ecd.figures.IElectricalFigure.LogicLevel;

public class Not extends Strategy {

	private static final long serialVersionUID = -9101447614432478568L;

	private static final String NAME = "NOT";
	private static final Integer DEF_INPUT_PINS = 1;

	static {
		StrategyFactory.getInstance().registerStrategy(Not.NAME, Not.class);
		StrategyFactory.getInstance().registerInput(Not.NAME,
				Not.DEF_INPUT_PINS);
	}

	@Override
	public boolean inputCountAlterable() {
		return false;
	}

	@Override
	public String strategyName() {
		return Not.NAME;
	}

	@Override
	protected LogicLevel getStrategyOutput(LogicLevel[] inputs) {
		LogicLevel ret;
		if (inputs[0].equals(LogicLevel.LOW))
			ret = LogicLevel.HIGH;
		else
			ret = LogicLevel.LOW;
		return ret;
	}
}
