package ecd.strategies;

import ecd.figures.IElectricalFigure.LogicLevel;

public class And extends Strategy {

	private static final long serialVersionUID = 7463422659356137250L;

	private static final String NAME = "AND";
	private static final Integer DEF_INPUT_PINS = 2;

	static {
		StrategyFactory.getInstance().registerStrategy(And.NAME, And.class);
		StrategyFactory.getInstance().registerInput(And.NAME,
				And.DEF_INPUT_PINS);
	}

	@Override
	public String strategyName() {
		return And.NAME;
	}

	@Override
	protected LogicLevel getStrategyOutput(LogicLevel[] inputs) {
		LogicLevel ret = LogicLevel.HIGH;
		for (LogicLevel l : inputs) {
			if (!l.equals(LogicLevel.HIGH)) {
				ret = LogicLevel.LOW;
				break;
			}
		}
		return ret;
	}
}
